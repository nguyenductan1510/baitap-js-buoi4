/**
 * EX 1
 * đầu vào: nhận vào 3 số nguyên 
 * xử lý: so sánh 3 số nguyên đó và sắp xếp theo thứ tự tăng dần
 * đầu ra: xuất ra kết quả cho người dùng
 */

function sapXepSTT(){
    var number1 = document.getElementById("sothu1").value * 1;
    var number2 = document.getElementById("sothu2").value * 1;
    var number3 = document.getElementById("sothu3").value * 1;
    
    var ketQua;
    if(number1 >= number2 && number2 >= number3){
        ketQua = `${number3},${number2},${number1}`
    }else if(number2 >= number3 && number3 >= number1){
        ketQua = `${number1},${number3},${number2}`
    }else if(number3 >= number1 && number1 >= number2){
        ketQua = `${number2},${number1},${number3}`
    }else if (number1 >= number3 && number3 >=number2){
        ketQua = `${number2} ,${number3},${number1}`
    }else if(number1 >= number2 && number1 <= number3){
        ketQua = `${number3},${number2}, ${number1}`
    }
    else if(number2 >= number1 && number1 >=number3){
        ketQua = `${number2},${number1}, ${number3}`

    }
    else {}
    document.getElementById("result").innerHTML = ketQua;

}

    /**
     * EX 2
     * Đầu vào:Nhận vào thông tin người dùng đã chọn
     * Xử lý: đưa ra câu chào ừ thồn tin người dùng chọn
     * Đầu ra: xuất câu chào ra màn hình của người dùng
     */


    function loiChao(){
        var thanhVien = document.getElementById("thanhvien").value;
        var loiChao;
        if(thanhVien == "B"){
            loiChao = `xin chào bố`
        }else if(thanhVien == "M"){
            loiChao = `xin chào Mẹ`
        }else if(thanhVien == "A"){
            loiChao = `xin chào anh trai`
        }else{
            loiChao = `xin chào em gái`
        }
        document.getElementById("guiloichao").innerHTML = loiChao;
    }

    /**
     * EX 3 
     * Đầu vào: nhận vào 3 con số nguyên bất kỳ
     * Xử lý: tính có bao nhiêu số chẵn và lẻ
     * Đầu ra: xuất ra tổng số chẵn và số lẻ tính được
     */
function demSoChanLe(){
    var soThu1 = document.getElementById("number1").value * 1;
    var soThu2 = document.getElementById("number2").value * 1;
    var soThu3 = document.getElementById("number3").value * 1;
    var soChan = 0;
    var soLe = 0;
if(soThu1 % 2 == 0){
    soChan +=1
}else{
    soLe +=1
}
if(soThu2 % 2 == 0){
    soChan +=1
}else{
    soLe +=1
}
if(soThu3 % 2 == 0){
    soChan +=1
}else{
    soLe +=1
}
    document.getElementById("tongso").innerHTML = ` số chẵn ${soChan}, số lẻ ${soLe}`
}
/**
 * Đầu vào:Nhận vào 3 cạnh mà người dùng đã nhập
 * Xử lý:tính toán và so sánh các cạnh để chọn ra kết quả phù hợp
 * Đầu ra: xuất ra tên tam giác
 */
function tamGiac(){
    var canh1 = document.getElementById("canh1").value*1;
    var canh2 = document.getElementById("canh2").value*1;
    var canh3 = document.getElementById("canh3").value*1;
    var tenTamGiac;
    if(canh1 == canh2 && canh2 == canh3){
        tenTamGiac = `Hình tam giác đều`
    }else if (canh1 == canh2 && canh2 > canh3){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh2 && canh1 > canh3){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh3 && canh1 > canh2){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh3 && canh3 > canh2){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh2 == canh3 && canh2 > canh1){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh2 == canh3 && canh3 > canh1){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh2 && canh2 < canh3){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh2 && canh1 < canh3){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh3 && canh1 < canh2){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh1 == canh3 && canh3 < canh2){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh2 == canh3 && canh2 < canh1){
        tenTamGiac = `Hình Tam giác cân`
    }else if (canh2 == canh3 && canh3 < canh1){
        tenTamGiac = `Hình Tam giác cân`
    }else{
        tenTamGiac = `Hình Tam giác Vuông`
    }

    document.getElementById("tamgiac").innerHTML = tenTamGiac;
}